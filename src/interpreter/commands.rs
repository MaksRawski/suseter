use super::*;

use std::convert::TryFrom;
use rand::prelude::random;
use getchar::getchar;

impl Interpreter{
    pub(super) fn red(&mut self){
        self.acc.0 += 1;
    }
    pub(super) fn blue(&mut self){
        self.stack.push(self.acc.0);
    }
    pub(super) fn purple(&mut self){
        self.stack.pop();
    }
    /// there are 2 cases where this function could fail
    /// either
    /// - stack is empty Err(Err( () ))
    /// or
    /// - value can't be fit into char Err(Ok( value ))
    pub(super) fn green(&self) -> Result<char, Error>{
        let value = self.stack.last();
        if value == None{
            return Err(Error::EmptyStack);
        }
        let value = value.unwrap();

        match u8::try_from(*value){
            Ok(c) => Ok(c as char),
            Err(e) => Err(Error::NoChar(*value)),
        }
    }
    pub(super) fn yellow(&mut self){
        self.stack.push(getchar().unwrap() as CellValue)
    }
    // TODO: return a warning when popping off of empty stack
    pub(super) fn cyan(&mut self){
        let num_of_pops = random::<CellValue>() % self.acc.0;
        for _ in 0..num_of_pops{
            self.stack.pop();
            // TODO: we could return error when trying
            // to pop values off of empty stack
        }
    }
    pub(super) fn black(&mut self) -> Option<&CellValue>{
        self.stack.last()
    }
    pub(super) fn white(&mut self){
        self.acc.0 -= 1;
    }
    pub(super) fn brown(&mut self) -> Result<(), ()>{
        let value = self.stack.last().ok_or(())?;
        self.acc.0 = *value;
        Ok(())
    }
    pub(super) fn lime(&mut self) -> Result<(), ()>{
        let v = self.stack.last_mut().ok_or(())?;
        *v += *v;
        Ok(())
    }
    pub(super) fn pink(&mut self){
        self.acc.0 = 0;
    }
    pub(super) fn orange(&mut self){
        self.acc.0 *= 10;
    }
    // TODO: use enum instead
    // - green returns a char or fails
    // - black returns a i32 value
    //
    // errors:
    // - stack is empty
    // - `green` character didn't fit
    /// Ok(String) will contain output
    /// Err(String) will contain error message
    pub(super) fn sus(&mut self, color: Color) -> Result<Option<String>, Error>{
        match color{
            Color::RED    => { self.red();    Ok(None) },
            Color::BLUE   => { self.blue();   Ok(None) },
            Color::PURPLE => { self.purple(); Ok(None) },
            Color::YELLOW => { self.yellow(); Ok(None) },
            Color::CYAN   => { self.cyan();   Ok(None) },
            Color::WHITE  => { self.white();  Ok(None) },
            Color::BROWN  => { self.brown();  Ok(None) },
            Color::LIME   => { self.lime();   Ok(None) },
            Color::PINK   => { self.pink();   Ok(None) },
            Color::ORANGE => { self.orange(); Ok(None) },
            Color::GREEN  => {
                match self.green(){
                }
            },
            Color::BLACK => {
                self
                    .black()
                    .ok_or(Error::EmptyStack)
                    .map(|v| Some( format!("{}", v) ))
            }
        }
    }
    pub(super) fn vented(&mut self){
        self.acc.1 *= 10;
    }
    pub(super) fn sussy(&mut self){
        self.acc.1 -= 1;
    }
    pub(super) fn electrical(&mut self){
        self.acc.1 = 0;
    }

    pub(super) fn should_start_loop(&self) -> bool{
        // TODO: could be done more concisely
        let mut stack = *self.stack.last().unwrap_or(&0);
        let mut acc = self.acc.1;

        if stack > 255{
            stack = 0;
        }
        if acc > 255{
            acc = 0;
        }
        stack == acc
    }

    pub(super) fn should_jump_up(&self) -> bool{
        !self.should_start_loop()
    }


    pub(super) fn end_loop(&mut self){
        if self.should_jump_up(){
            self.pushingToStack = false;
            self.skippingLoop = false;

            for stack_cmd in self
                .expressionStack
                .last()
                .unwrap_or(&Vec::new())
                .iter()
            {
                self.interpret(stack_cmd);
            }

        }
    }
}
