use crate::types::*;
use crate::expression::*;

pub type CellValue = i32;

pub struct Interpreter{
    stack: Vec<CellValue>,
    acc: (CellValue, CellValue),
    lastColor: Option<Color>,
    expressionStack: Vec<Vec<Expression>>,
    pushingToStack: bool,
    skippingLoop: bool,
}

impl Interpreter{
    pub fn new() -> Self{
        Self{
            stack: Vec::new(),
            acc: (0, 0),
            lastColor: None,
            expressionStack: Vec::new(),
            pushingToStack: false,
            skippingLoop: false,
        }
    }
}

mod commands;
mod interpreter;
