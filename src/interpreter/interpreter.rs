use super::*;

use crate::expression::{Color, Command, Expression};

impl Interpreter{
    /// * Will return None if a word isn't either a color or a command.
    /// * Parsing is context dependent as command "SUS" executes last color.
    pub fn parse(&mut self, word: &str) -> Option<Expression>{
        let color = match word{
            "RED"    => Some(Color::RED),
            "BLUE"   => Some(Color::BLUE),
            "PURPLE" => Some(Color::PURPLE),
            "GREEN"  => Some(Color::GREEN),
            "YELLOW" => Some(Color::YELLOW),
            "CYAN"   => Some(Color::CYAN),
            "BLACK"  => Some(Color::BLACK),
            "WHITE"  => Some(Color::WHITE),
            "BROWN"  => Some(Color::BROWN),
            "LIME"   => Some(Color::LIME),
            "PINK"   => Some(Color::PINK),
            "ORANGE" => Some(Color::ORANGE),
            _ => self.lastColor.clone(),
        };
        let command = match word{
            "SUS"        => Some(Command::SUS),
            "VENTED"     => Some(Command::VENTED),
            "SUSSY"      => Some(Command::SUSSY),
            "ELECTRICAL" => Some(Command::ELECTRICAL),
            "WHO"        => Some(Command::WHO),
            "WHERE"      => Some(Command::WHERE),
            _ => None
        };

        if color.is_none() && command.is_none() {
            None
        } else {
            self.lastColor = color.clone();
            color.map(|c| c.into()).or(command.map(|cmd| cmd.into()))
        }
    }

    pub fn execute(&mut self, expr: &Expression) -> Result<Option<String>, Error>{
        // TODO: this function is way too complex
        if self.skippingLoop {
            if *expr == Expression::Command(Command::WHERE) {
                self.expressionStack.pop();
                self.skippingLoop == false;
            }
            return Ok(None);
        }

        if self.pushingToStack{
            self.expressionStack.last_mut().unwrap().push(expr.clone());
        }

        if let Expression::Command(cmd) = expr{
            match cmd{
                Command::WHO => {
                    if self.should_start_loop(){
                        self.skippingLoop = false;
                        self.expressionStack.push(Vec::new());
                    } else{
                        self.skippingLoop = true;
                    }
                    Ok(None)
                }
                Command::WHERE => {
                    self.end_loop();
                    Ok(None)
                }
                _ => { panic!("TODO: implement entire pattern you dummy") }
            }
        } else if let Expression::Color(color) = expr{
            // probably not necessary to do it here since it's
            // done when parsing
            // self.lastColor = Some(*color);
            Ok(None)
        } else{
            unreachable!();
        }
    }
}
