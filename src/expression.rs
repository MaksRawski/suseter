#[derive(Clone, PartialEq)]
pub enum Expression{
    Color(Color),
    Command(Command),
}

pub enum Error{
    EmptyStack,
    NoChar(i32),
}

#[derive(Copy, Clone, PartialEq)]
pub enum Color {
    RED,    // Increment accumulator 1.
    BLUE,   // Push the value in accumulator 1 to the top of the stack.
    PURPLE, // Pop the value at the top of the stack off of the stack.
    GREEN,  // Output the ASCII character associated with the number at the top of the stack.
    YELLOW, // Push a byte of input to the top of the stack.
    CYAN,   // Pop the value off of the top of the stack a random amount of times, the highest possible amount of times is equal to the value in accumulator 1.
    BLACK,  // Output the value at the top of the stack.
    WHITE,  // Decrement accumulator 1.
    BROWN,  // Set the value in accumulator 1 to the value at the top of the stack.
    LIME,   // Adds the value at the top of the stack to the value at the top of the stack.
    PINK,   // Set the value in accumulator 1 to 0.
    ORANGE, // Increment accumulator 1 10 times.
}

impl Into<Expression> for Color{
    fn into(self) -> Expression{
        Expression::Color(self)
    }
}

#[derive(Copy, Clone, PartialEq)]
pub enum Command {
    SUS,        // Execute command associated with colour.
    VENTED,     // Increment accumulator 2 10 times.
    SUSSY,      // Decrement accumulator 2.
    ELECTRICAL, // Set the value in accumulator 2 to 0.
    WHO,        // Go past the next "WHERE" if the value at the top of the stack is equal to the value in accumulator 2.
    WHERE,      // Go to last "WHO" if the value at the top of the stack is not equal to the value in accumulator 2.
}

impl Into<Expression> for Command{
    fn into(self) -> Expression{
        Expression::Command(self)
    }
}
