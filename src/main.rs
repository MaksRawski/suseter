use std::fs;
use clap::{App, Arg};

fn main() {
    let args = App::new("suseter")
        .about("There is a sus interpreter among us.")
        .version("0.1.0")
        .arg(Arg::with_name("file")
            .required(true)
        )
        .get_matches();

    let file_name = args.value_of("file").unwrap();
    let program = fs::read_to_string(file_name).unwrap();
}
