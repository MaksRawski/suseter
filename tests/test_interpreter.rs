use suseter::Interpreter;

#[test]
fn test_print_a(){
    let mut interpreter = Interpreter::new();
    // print "A"
    let program = "red sus sus sus sus sus sus orange sus red sus sus sus sus sus green sus";

    // this should be private:
    // interpreter.blue();
    for word in program.split(" "){
        let expr = interpreter.parse(word).unwrap();
        interpreter.execute(&expr);
    }
}
